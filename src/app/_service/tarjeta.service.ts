import { Injectable } from '@angular/core';
import { Tarjeta } from '../_model/tarjeta';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable() //Para inyectar esta case en las otras capas
/*** Hay que pasarle los tockens a esta clase** */
export class TarjetaService { // registrar en app.module ->providers

  url:string =  `${HOST}/tarjeta`; //<-Ahora 
  tarjetaCambio = new Subject<Tarjeta[]>();
  mensaje = new Subject<string>();


  constructor(private http:HttpClient) { 

  }
  getlistarTarjeta(){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Tarjeta[]>(`${this.url}/listar`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    }); 

  }
  registrar(tarjeta:Tarjeta){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(`${this.url}/registrar`,tarjeta,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });   
   }
   modificar(tarjeta:Tarjeta){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(`${this.url}/actualizar`,tarjeta,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });  
   }
   eliminar(tarjeta:Tarjeta){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete(`${this.url}/eliminar/${tarjeta.tarjeta_id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });  
   }
   getTarjetaPorId(id:number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Tarjeta>(`${this.url}/listar/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    }); 
   }
}