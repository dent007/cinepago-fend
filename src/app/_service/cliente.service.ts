import { Injectable } from '@angular/core';
import { Cliente } from '../_model/cliente';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable() //Para inyectar esta case en las otras capas
/*** Hay que pasarle los tockens a esta clase** */
export class ClienteService { // registrar en app.module ->providers

  //url:string =  HOST + '/cliente'; //<-antes
  url:string =  `${HOST}/cliente`; //<-Ahora 
  clienteCambio = new Subject<Cliente[]>();
  mensaje = new Subject<string>();

  //clientes: Cliente[]=[];

  constructor(private http:HttpClient) { 

    /*let p = new Cliente(); 
    
    p.cliente_id=1;
    p.nombre="Luis";
    p.apellido="Cordo";
    p.documento=12345678;
    this.clientes.push(p);

    p=new Cliente();
    p.cliente_id=2;
    p.nombre="Amparo";
    p.apellido="Nuñez";
    p.documento=111111;
    this.clientes.push(p);*/
  }
  getlistarCliente(p:number, s:number){
   // de tod el json tocken guardado en TOKEN_NAME traerme el campo:valor de access_token 
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Cliente[]>(`${this.url}/listarPageable?page=${p}&size=${s}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    }); 
    //return this.clientes;
  }
  registrar(cliente:Cliente){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(`${this.url}/registrar`,cliente,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });   
   }
   modificar(cliente:Cliente){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(`${this.url}/actualizar`,cliente,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });  
   }
   eliminar(cliente:Cliente){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    //console.log("ACa"+cliente.cliente_id);
    return this.http.delete(`${this.url}/eliminar/${cliente.cliente_id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });  
   }
   getClientePorId(id:number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Cliente>(`${this.url}/listar/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    }); 
   }
   getlistarClienteE(){
    // de tod el json tocken guardado en TOKEN_NAME traerme el campo:valor de access_token 
     let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
     return this.http.get<Cliente[]>(`${this.url}/listar`,{
       headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
       
     });
}
} 
/*
http://localhost:8080/cliente/listarPageable?page=4&size=2

{
    "content": [
        {
            "cliente_id": 15,
            "documento": 1111111111,
            "nombre": "n",
            "apellido": "a",
            "created": null,
            "update": null
        },
        {
            "cliente_id": 17,
            "documento": 9999,
            "nombre": "NK",
            "apellido": "RS",
            "created": null,
            "update": null
        }
    ],
    "totalPages": 10,
    "totalElements": 20,
    "last": false,
    "numberOfElements": 2,
    "first": false,
    "sort": null,
    "size": 2,
    "number": 4
}
*/