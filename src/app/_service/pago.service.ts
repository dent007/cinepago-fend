import { Injectable } from '@angular/core';
import { Pago } from '../_model/pago';
import { HOST, TOKEN_NAME } from '../_shared/var.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable() //Para inyectar esta case en las otras capas
export class PagoService { // registrar en app.module ->providers

  url:string =  `${HOST}/pago`; //<-Ahora 
  pagoCambio = new Subject<Pago[]>();
  mensaje = new Subject<string>();


  constructor(private http:HttpClient) { 

  }
  //getlistarPago(p:number, s:number){
    getlistarPago(){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    //return this.http.get<Pago[]>(`${this.url}/listarPageable?page=${p}&size=${s}`,{
      return this.http.get<Pago[]>(`${this.url}/listar`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    }); 

  }
  registrar(pago:Pago){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post(`${this.url}/registrar`,pago,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });   
   }
   modificar(pago:Pago){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put(`${this.url}/actualizar`,pago,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });  
   }
   eliminar(pago:Pago){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete(`${this.url}/eliminar/${pago.pago_id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    });  
   }
   getPagoPorId(id:number){
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Pago>(`${this.url}/listar/${id}`,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
      
    }); 
   }
}
