import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

  import { LoginService } from './../_service/login.service';
//import { LoginService } from '../_service/login.service';
//import { TOKEN_NAME } from '../_shared/var.constant';
import { TOKEN_NAME } from './../_shared/var.constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  contrasena: string;
  //Inyecto
  constructor(private loginService: LoginService, private router: Router) { }
   


  ngOnInit() {
  }
  
  iniciarSesion() {//login se comunica con http://localhost:8080/oauth/token y trae/mete TODO ese JSON en data
    this.loginService.login(this.usuario, this.contrasena).subscribe(data => {      
      //
      if (data) {
        //JSON.stringify() convierte un objeto o valor de JavaScript "data"en una cadena de texto JSON
        let token = JSON.stringify(data);

//sessionStorage almacena/guarda datos en pag mientras pestaña este open. sessionStorage.setItem('Nombre', 'Miguel Antonio')
          //https://alligator.io/js/introduction-localstorage-sessionstorage/
        sessionStorage.setItem(TOKEN_NAME, token);//setItem añade "clave:valor" al almacen/Storage o los actualiza
        //pasado todo, navego a cliente
        this.router.navigate(['cliente']);
      }
    });
  }
  cerrarSesion() {    
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
}
//revisado