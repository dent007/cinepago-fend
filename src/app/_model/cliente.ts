export class Cliente{

    cliente_id: number;
    nombre: string;
    apellido: string;
    documento: number;
    created: Date;
    update:Date;
    
    /** "created": "1989-11-11T05:30:45",
    "update": "1989-11-11T05:30:45"**/
} 