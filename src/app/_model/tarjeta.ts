 import { Cliente } from './cliente';
export class Tarjeta{

    public tarjeta_id: number;
    public numero: number;
    public tipo: string;
    public cliente: Cliente;
    public saldo: number;
    public created: Date;
    public update: Date;

}