import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ClienteService } from './_service/cliente.service';
import { HttpClientModule } from '@angular/common/http';
import { ClienteEdicionComponent } from './pages/cliente/cliente-edicion/cliente-edicion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { LoginService } from './_service/login.service';

import { TarjetaService } from './_service/tarjeta.service';
import { PagoService } from './_service/pago.service';
import { GuardService } from './_service/guard.service';
import { TarjetaComponent } from './pages/tarjeta/tarjeta.component';
import { PagoComponent } from './pages/pago/pago.component';
import { PagoEdicionComponent } from './pages/pago/pago-edicion/pago-edicion.component';
import { TarjetaEdicionComponent } from './pages/tarjeta/tarjeta-edicion/tarjeta-edicion.component';


@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    ClienteEdicionComponent,
    LoginComponent,
    TarjetaComponent,
    PagoComponent,
    PagoEdicionComponent,
    TarjetaEdicionComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,    
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ClienteService, LoginService, GuardService,TarjetaService,PagoService],
  bootstrap: [AppComponent]
})
export class AppModule { }//Revisado