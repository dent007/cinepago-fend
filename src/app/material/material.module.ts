import { MatPaginatorImpl } from './../_shared/mat-paginator';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule, MatCardModule, MatInputModule, MatButtonModule, MatIconModule, 
  MatMenuModule, MatSidenavModule, MatDividerModule, MatToolbarModule, MatFormFieldModule, 
  MatTableModule, MatPaginatorModule, MatSortModule, MatPaginatorIntl, MatSelectModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatDividerModule,
    MatToolbarModule,
    MatFormFieldModule, 
    MatTableModule, 
    MatPaginatorModule, 
    MatSortModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    MatSelectModule

  ],
  exports:[
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatDividerModule,
    MatToolbarModule,
    MatFormFieldModule, 
    MatTableModule, 
    MatPaginatorModule, 
    MatSortModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    MatSelectModule
  ],
  //gestiona  idioma en paginadores  _shared->mat-paginaor.ts
  providers:[{provide:MatPaginatorIntl,useClass:MatPaginatorImpl }],

  declarations: []
})
export class MaterialModule { }
//Revisado