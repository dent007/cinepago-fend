import { Tarjeta } from './../../../_model/tarjeta';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { TarjetaService } from '../../../_service/tarjeta.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-tarjeta-edicion',
  templateUrl: './tarjeta-edicion.component.html',
  styleUrls: ['./tarjeta-edicion.component.css']
})
export class TarjetaEdicionComponent implements OnInit {


  id:number;
  tarjeta:Tarjeta;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
  constructor(private tarjetaService:TarjetaService, private route: ActivatedRoute, private router: Router) { 
    this.tarjeta = new Tarjeta();
    this.form = new FormGroup({
    "id":new FormControl(0),
    'cliente':new FormControl(''),
    'numero':new FormControl(''),
    'tipo':new FormControl(''),
    'saldo':new FormControl('')
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params)=>{
      this.id= params['id'];
      this.edicion=params['null'] !=null;
      this.initForm();
    });
  }
  private initForm(){
     if(this.edicion){
      this.tarjetaService.getTarjetaPorId(this.id).subscribe(data=>{
        let id=data.tarjeta_id;
        let numero=data.numero;
        let tipo=data.tipo;
        let cliente=data.cliente;
        let saldo=data.saldo;

        this.form = new FormGroup({
          'id':new FormControl(id),
          'numero':new FormControl(numero),
          'tipo':new FormControl(tipo),
          'cliente':new FormControl(cliente),
          'saldo':new FormControl(saldo)
        });        
      });
     }
  }
  operar(){
      this.tarjeta.tarjeta_id=this.form.value['id'];
      this.tarjeta.numero=this.form.value['numero'];
      this.tarjeta.tipo=this.form.value['tipo'];
      this.tarjeta.cliente=this.form.value['cliente'];
      this.tarjeta.saldo=this.form.value['saldo'];

      if(this.tarjeta !=null && this.tarjeta.tarjeta_id >0){
        this.tarjetaService.modificar(this.tarjeta).subscribe(data=>{
          if(data===1){
            this.tarjetaService.getlistarTarjeta().subscribe(pago=>{
              this.tarjetaService.tarjetaCambio.next(pago);
              this.tarjetaService.mensaje.next("se modifico");
            });  
          }else{
            this.tarjetaService.mensaje.next("No se pudo modificar");
          }
        });
      }else{
        this.tarjetaService.registrar(this.tarjeta).subscribe(data=>{
          if(data===1){
            this.tarjetaService.getlistarTarjeta().subscribe(pago=>{
              this.tarjetaService.tarjetaCambio.next(pago); 
              this.tarjetaService.mensaje.next("Se registro");
            });  
          }else{
            this.tarjetaService.mensaje.next("No se pudo registrar");
          }
        });
      }
      /*
      this.pagoService.getlistarPago().subscribe(data=>{
        this.pagoService.pagoCambio.next(data);
      });*/
      this.router.navigate(['pago']);
  }
}




//Revisado
