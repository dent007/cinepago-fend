import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Tarjeta } from '../../_model/tarjeta';
import { TarjetaService } from '../../_service/tarjeta.service';


@Component({
  selector: 'app-tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.css']
})

export class TarjetaComponent implements OnInit {

  //lista: Pago[]=[];
  //maxFecha: Date = new Date();
  
  /*Inico las columnas a mostar*/
  displayedColumns = ['tarjeta_id', 'numero', 'tipo', 'cliente', 'saldo', 'created','update','acciones'];
  
  dataSource:MatTableDataSource<Tarjeta>;

  /*@ViewChild buscará en html anotaciones "MatPaginator y MatSort",dentro de su html*/ 
  /*tejer html y ts*/
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;
  cantidad: number;
  
  constructor(private tarjetaService:TarjetaService,private snackBar: MatSnackBar, public route:ActivatedRoute) { }

  
  ngOnInit() {
    
    this.tarjetaService.tarjetaCambio.subscribe(data=>{ // Observa cambios      
//      this.dataSource = new MatTableDataSource(pagos);     
      this.dataSource = new MatTableDataSource(data);     
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;

      //let pagos = JSON.parse(JSON.stringify(data)).content;
      //this.cantidad = JSON.parse(JSON.stringify(data)).totalElements; 
      
     

    });

    this.tarjetaService.getlistarTarjeta().subscribe(data=>{ // me susucribo al servicio "get.." en espera que  me de la data
     /* let pagos= JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(pagos);*/
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

    });
     
     
   /*   this.pagoService.mensaje.subscribe(data => {
        this.snackBar.open(data, null, { duration: 2000 });
      });*/

  }
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(tarjeta:Tarjeta):void{
    this.tarjetaService.eliminar(tarjeta).subscribe(data=>{
      if(data===1){
         this.tarjetaService.getlistarTarjeta().subscribe(data=>{
           this.tarjetaService.tarjetaCambio.next(data);    
           this.tarjetaService.mensaje.next("Se eliminó correctamente");
         }); 
      }else{
           this.tarjetaService.mensaje.next("No se pudo eliminar"); 
      }
      this.snackBar.open(this.mensaje,null,{
        duration:2000,
      });
    });
  }

 /* mostrarMas(e){ //e.pageIndex e.pageSize e.length //console.log(e.pageIndex); //consele.log(e.pageSize);

    this.pagoService.getlistarPago(e.pageIndex,e.pageSize).subscribe(data=>{ //Cambio en siguiente pag, sgte pag,..
      console.log(data);
      let pagos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad=JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(pagos  );
      ///-->ver revisar
        //this.dataSource.paginator=this.paginator;
       //this.dataSource.sort=this.sort;
      ///<--ver
    });
  }*/
}

//Revisado
