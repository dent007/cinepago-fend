import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PagoService } from '../../../_service/pago.service';
import { Pago } from '../../../_model/pago';

@Component({
  selector: 'app-pago-edicion',
  templateUrl: './pago-edicion.component.html',
  styleUrls: ['./pago-edicion.component.css']
})
export class PagoEdicionComponent implements OnInit {

  id:number;
  pago:Pago;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente
  
  constructor(private pagoService:PagoService, private route: ActivatedRoute, private router: Router) { 
    this.pago = new Pago();
    this.form = new FormGroup({
    "id":new FormControl(0),
    'tarjeta':new FormControl(''),
    'importe':new FormControl(''),
    'pagoUuid':new FormControl(''),
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params)=>{
      this.id= params['id'];
      this.edicion=params['null'] !=null;
      this.initForm();
    });
  }
  private initForm(){
     if(this.edicion){
      this.pagoService.getPagoPorId(this.id).subscribe(data=>{
        let id=data.pago_id;
        let tarjeta=data.tarjeta;
        let importe=data.importe;
        let pagoUuid=data.pagoUuid;

        this.form = new FormGroup({
          'id':new FormControl(id),
          'tarjeta':new FormControl(tarjeta),
          'importe':new FormControl(importe),
          'pagoUuid':new FormControl(pagoUuid)
        });        
      });
     }
  }
  operar(){
      this.pago.pago_id=this.form.value['id'];
      this.pago.tarjeta=this.form.value['tarjeta'];
      this.pago.importe=this.form.value['importe'];
      this.pago.pagoUuid=this.form.value['pagoUuid'];
      if(this.pago !=null && this.pago.pago_id >0){
        this.pagoService.modificar(this.pago).subscribe(data=>{
          if(data===1){
            this.pagoService.getlistarPago().subscribe(pago=>{
              this.pagoService.pagoCambio.next(pago);
              this.pagoService.mensaje.next("se modifico");
            });  
          }else{
            this.pagoService.mensaje.next("No se pudo modificar");
          }
        });
      }else{
        this.pagoService.registrar(this.pago).subscribe(data=>{
          if(data===1){
            this.pagoService.getlistarPago().subscribe(pago=>{
              this.pagoService.pagoCambio.next(pago); 
              this.pagoService.mensaje.next("Se registro");
            });  
          }else{
            this.pagoService.mensaje.next("No se pudo registrar");
          }
        });
      }
      /*
      this.pagoService.getlistarPago().subscribe(data=>{
        this.pagoService.pagoCambio.next(data);
      });*/
      this.router.navigate(['pago']);
  }
}
