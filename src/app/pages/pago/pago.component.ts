import { Pago } from './../../_model/pago';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { PagoService } from '../../_service/pago.service';


@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css']
})

export class PagoComponent implements OnInit {

  //lista: Pago[]=[];
  //maxFecha: Date = new Date();
  
  /*Inico las columnas a mostar*/
  displayedColumns = ['pago_id', 'tarjeta', 'importe', 'pagoUuid', 'created','update','acciones'];
  

  dataSource:MatTableDataSource<Pago>;

  /*@ViewChild buscará en html anotaciones "MatPaginator y MatSort",dentro de su html*/ 
  /*tejer html y ts*/
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  mensaje: string;
  cantidad: number;
  
  //Inyectando un ClienteService
  constructor(private pagoService:PagoService,private snackBar: MatSnackBar, public route:ActivatedRoute) { }

  
  ngOnInit() {
    
    this.pagoService.pagoCambio.subscribe(data=>{ // Observa cambios      
      this.dataSource = new MatTableDataSource(data);     
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort= this.sort;


    });

    this.pagoService.getlistarPago().subscribe(data=>{ // me susucribo al servicio "get.." en espera que  me de la data
     // console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
      console.log(data);

    });
     
     


  }
  applyFilter(filterValue:string ){ // recibe texto     
    filterValue = filterValue.trim(); // limpia espacios en blanco
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue; // busca texto 
  }

  eliminar(pago:Pago):void{
    this.pagoService.eliminar(pago).subscribe(data=>{
      if(data===1){
         this.pagoService.getlistarPago().subscribe(data=>{
           this.pagoService.pagoCambio.next(data);    
           this.pagoService.mensaje.next("Se eliminó correctamente");
         }); 
      }else{
           this.pagoService.mensaje.next("No se pudo eliminar"); 
      }
      this.snackBar.open(this.mensaje,null,{
        duration:2000,
      });
    });
  }

}

//Revisado
