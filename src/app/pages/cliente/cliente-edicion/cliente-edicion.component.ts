 import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Cliente } from '../../../_model/cliente';
import { ClienteService } from '../../../_service/cliente.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-cliente-edicion',
  templateUrl: './cliente-edicion.component.html',
  styleUrls: ['./cliente-edicion.component.css']
})
export class ClienteEdicionComponent implements OnInit {
  
  id:number;
  cliente:Cliente;
  form: FormGroup;
  edicion: boolean=false; //edicion o nuevo cliente

  constructor(private clienteService:ClienteService, private route: ActivatedRoute, private router: Router) {
    //instancio el cliente a usar
    this.cliente= new Cliente();


    //indico la estuctura de formulario que el html tiene "lo modelo"
    //paso valores en blanco hacia el formulario 
    this.form = new FormGroup({
    'id':new FormControl(0),
    'nombre':new FormControl(''),
    'apellido':new FormControl(''),
    'documento':new FormControl('')
  });
}
  // lo primero que se exe despué s del constructor
  ngOnInit() { //Apenas cargue este componente, captura el cliente_id de la url 
    this.route.params.subscribe((params: Params) => {// params es un Observable
      this.id=params['id']; // id declarado como parámetro en la url
      // si id existe, user pide actualizar , sino crear
      this.edicion = params['id'] != null; // si encontró id, edición = true, sino false
      this.initForm(); 
    });
  }
  private initForm() {
    if(this.edicion) { //soo si hay id entra en este bloque, sino no hace nada, escapa de if
      this.clienteService.getClientePorId(this.id).subscribe(data => {
        //obtengo los valores desde el Servicio quién recibio json con data de backEnd
        let id =  data.cliente_id;
        let nombre = data.nombre;
        let apellido = data.apellido;
        let documento = data.documento;

        //Paso los valores poblados hacia el formulario
        this.form = new FormGroup({
          'id':new FormControl(id),
          'nombre':new FormControl(nombre),
          'apellido':new FormControl(apellido),
          'documento':new FormControl(documento)
        });
      });
    }
  }
  operar(){ // Capturo contenido de cajas/campos  de formulario y vinculado con formControlName
    this.cliente.cliente_id = this.form.value['id'];
    this.cliente.nombre = this.form.value['nombre'];
    this.cliente.apellido = this.form.value['apellido'];
    this.cliente.documento = this.form.value['documento'];
   // if (this.cliente !=null && this.cliente.cliente_id >0) { // viene algo, un cliente existente
   if (this.edicion) {  // si hay algo que editar  
   //UPDATE o MODIFICAR
      this.clienteService.modificar(this.cliente).subscribe(data =>{  //data tiene lo que entrega backend
        console.log(data); 

        if(data ===1){//el backEnd retorna 1 = se modificó bien
          this.clienteService.getlistarCliente(0,5).subscribe(clientes =>{
            this.clienteService.clienteCambio.next(clientes);
            this.clienteService.mensaje.next('Está modificado');
            });
        }else{//el backEnd retorna 0 = se modificó mal
          this.clienteService.mensaje.next('No está modificado');

        }
      });
    } else { // No viene nada,   es null y < 0 ese cliente id
      //INSERT
      this.clienteService.registrar(this.cliente).subscribe(data =>{
        console.log(data); 

        if(data ===1){//el backEnd retorna 1 = se insertó bien
          
          this.clienteService.getlistarCliente(0,5).subscribe(clientes =>{
            this.clienteService.clienteCambio.next(clientes);
            this.clienteService.mensaje.next('Está registrado');
           });

        }else{//el backEnd retorna 0 = se insertó mal
          this.clienteService.mensaje.next('No está registrado');

        }
      });
    }
    //Luego de insetar o modificar navegar a cliente
    this.router.navigate(['cliente'])

  } 
}
//Revisado