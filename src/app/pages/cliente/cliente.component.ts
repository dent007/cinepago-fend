import { Component, OnInit, ViewChild } from '@angular/core';
import { Cliente } from '../../_model/cliente';
import { ClienteService } from '../../_service/cliente.service';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  //lista: Cliente[]=[];
  //maxFecha: Date = new Date();
  
  /*Inico las columnas a mostar*/
  displayedColumns = ['cliente_id', 'nombre', 'apellido', 'documento', 'created','update','acciones'];
  
  dataSource:MatTableDataSource<Cliente>;

  /*@ViewChild buscará en html anotaciones "MatPaginator y MatSort",dentro de su html*/ 
  /*tejer html y ts*/
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  cantidad: number;
  mensaje:string;
  
  //Inyectando un ClienteService
  constructor(private clienteService:ClienteService,private snackBar: MatSnackBar) { }

  ngOnInit() {

    //retorno obj Observabe y dentro mis datos. subscribe: obtiene la nf qesta dentro de observable la pone en data
    this.clienteService.getlistarCliente(0,5).subscribe(data=>{ // me susucribo al servicio "get.." en espera que me de la data
      //this.lista=data; // data es arrego de cliente
      let clientes= JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(clientes);
      //this.dataSource.paginator=this.paginator;//en está parte Angular gestiona e paginador
      this.dataSource.sort=this.sort;

    });
    this.clienteService.clienteCambio.subscribe(data=>{ // Observa cambios      
      let clientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements; 
      
      this.dataSource = new MatTableDataSource(clientes);     
     // this.dataSource.paginator=this.paginator;
     // this.dataSource.sort=this.sort;

    });  
     
      this.clienteService.mensaje.subscribe(data => {
        this.snackBar.open(data, null, { duration: 2000 });
      });

  }
  /*Para majenar el filtro "buscar cliente" */
  applyFilter(filterValue:string ){ // recibe texto
     
    filterValue = filterValue.trim(); // limpia espacios en blanco

    /*Convertir a minuscula lo ingresado */
    filterValue = filterValue.toLowerCase();
    
    this.dataSource.filter = filterValue; // busca texto 
  }
  mostrarMas(e){ //e.pageIndex e.pageSize e.length //console.log(e.pageIndex); //consele.log(e.pageSize);

    this.clienteService.getlistarCliente(e.pageIndex,e.pageSize).subscribe(data=>{ //Cambio en siguiente pag, sgte pag,..
      console.log(data);
      //Convierto a string la respuesta del servicio "data", luego a JSON pa extraer solo la parte que me sirve  
      let clientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad=JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(clientes);
      ///-->ver revisar
        //this.dataSource.paginator=this.paginator;
       //this.dataSource.sort=this.sort;
      ///<--ver
    });
  }

  eliminar(cliente:Cliente):void{
    this.clienteService.eliminar(cliente).subscribe(data=>{
      console.log(cliente.cliente_id);
      if(data===1){
         this.clienteService.getlistarCliente(0,5).subscribe(data=>{
           this.clienteService.clienteCambio.next(data);    
           this.clienteService.mensaje.next("Se eliminó correctamente");
         }); 
      }else{
           this.clienteService.mensaje.next("No se pudo eliminar"); 
      }
   //   this.snackBar.open(this.mensaje,null,{
     //   duration:2000,
     // });
    });
  }

}

//Revisado
/*
http://localhost:8080/cliente/listarPageable?page=4&size=2

{
    "content": [
        {
            "cliente_id": 15,
            "documento": 1111111111,
            "nombre": "n",
            "apellido": "a",
            "created": null,
            "update": null
        },
        {
            "cliente_id": 17,
            "documento": 9999,
            "nombre": "NK",
            "apellido": "RS",
            "created": null,
            "update": null
        }
    ], 
    "totalPages": 10, // Se armaron 7 páginas con los 20 elementos totales  de esta tabla 
    "totalElements": 20,
    "last": false,
    "numberOfElements": 2,
    "first": false,
    "sort": null,
    "size": 2,
    "number": 4
}
*/