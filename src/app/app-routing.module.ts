import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ClienteEdicionComponent } from './pages/cliente/cliente-edicion/cliente-edicion.component';

import { GuardService } from './_service/guard.service';
import { LoginComponent } from './login/login.component';
import { PagoComponent } from './pages/pago/pago.component';
import { TarjetaComponent } from './pages/tarjeta/tarjeta.component';
import { PagoEdicionComponent } from './pages/pago/pago-edicion/pago-edicion.component';
import { TarjetaEdicionComponent } from './pages/tarjeta/tarjeta-edicion/tarjeta-edicion.component';

const routes: Routes = [
 
  /*{ path: 'pago', component: PagoComponent, canActivate: [GuardService] },
  { path: 'tarjeta', component: TarjetaComponent, canActivate: [GuardService] },
*/
  { path: 'pago', component: PagoComponent,
    children:[
              { path:'nuevo',component: PagoEdicionComponent},
              { path:'edicion/:id',component: PagoEdicionComponent}// :id valor dinámmico enviado por url   
            ], 
    canActivate: [GuardService] // protegerá a cliente e hijos
  },

  { path: 'tarjeta', component: TarjetaComponent,
  children:[
            { path:'nuevo',component: TarjetaEdicionComponent},
            { path:'edicion/:id',component: TarjetaEdicionComponent}// :id valor dinámmico enviado por url   
          ], 
  canActivate: [GuardService] // protegerá a cliente e hijos
},

  { path: 'cliente', component: ClienteComponent,
    children:[
              { path:'nuevo',component: ClienteEdicionComponent},
              { path:'edicion/:id',component: ClienteEdicionComponent}// :id valor dinámmico enviado por url   
            ], 
    canActivate: [GuardService] // protegerá a cliente e hijos
  },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

//revisado!!